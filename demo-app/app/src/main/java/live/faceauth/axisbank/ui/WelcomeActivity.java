package live.faceauth.axisbank.ui;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.squareup.picasso.Picasso;
import live.faceauth.axisbank.R;
import live.faceauth.sdk.FaceAuth;

public class WelcomeActivity extends AppCompatActivity implements FaceAuth.FeedbackCallback {
  @BindView(R.id.ratingBar) RatingBar ratingBar;
  @BindView(R.id.feedback) EditText feedbackView;
  @BindView(R.id.name) TextView nameView;
  @BindView(R.id.phoneNumber) TextView phoneNumberView;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_welcome);
    ButterKnife.bind(this);
    feedbackView.setImeOptions(EditorInfo.IME_ACTION_DONE);
    feedbackView.setRawInputType(InputType.TYPE_CLASS_TEXT);
    String name = getIntent().getStringExtra("NAME");
    if (name != null) nameView.setText("Welcome, " + name);
    String phoneNumber = getIntent().getStringExtra("PHONE_NUMBER");
    if (phoneNumber != null) phoneNumberView.setText("Ph: " + phoneNumber);
    String imageUrl = getIntent().getStringExtra("PROFILE_PICTURE");
    if (imageUrl != null) {
      Picasso.with(this)
          .load(imageUrl)
          .placeholder(R.drawable.profile_placeholder)
          .into((ImageView) findViewById(R.id.profilePicture));
    }
  }

  @OnClick(R.id.submitFeedback) public void submitFeedback() {
    String requestId = getIntent().getStringExtra("REQUEST_ID");
    if (requestId == null) {
      requestId = getIntent().getStringExtra("PHONE_NUMBER");
    }
    int rating = Math.round(ratingBar.getRating());
    String feedback = feedbackView.getText().toString();
    if (rating == 0) {
      askForRating();
      return;
    }
    FaceAuth.getInstance().sendFeedback(requestId, rating, feedback, this);
  }

  private void askForRating() {
    new AlertDialog.Builder(this).setTitle("Select Rating")
        .setMessage("Please select a rating to submit the feedback")
        .setPositiveButton("OK", null)
        .show();
  }

  @OnClick(R.id.logout) public void logout() {
    final ProgressDialog progress = ProgressDialog.show(this, "Please wait", "Logging out..");
    progress.setCancelable(false);
    new Handler().postDelayed(new Runnable() {
      @Override public void run() {
        progress.dismiss();
        startActivity(new Intent(WelcomeActivity.this, SplashActivity.class));
        finish();
      }
    }, 500);
  }

  @Override public void onSuccess() {
    new AlertDialog.Builder(this).setTitle("Feedback Submitted")
        .setMessage("Thanks for your valuable feedback!")
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
          @Override public void onClick(DialogInterface dialog, int which) {
            ratingBar.setRating(0);
            feedbackView.setText("");
          }
        })
        .show();
  }

  @Override public void onFailure(String message) {
    new AlertDialog.Builder(this).setTitle("Feedback Failed")
        .setMessage("Failed to submit feedback. Please try again!\n" + message)
        .setPositiveButton("OK", null)
        .show();
  }

  @Override protected void onPause() {
    super.onPause();
    finish();
  }
}
