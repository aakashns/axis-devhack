package live.faceauth.axisbank.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import live.faceauth.axisbank.R;
import live.faceauth.sdk.FaceAuth;
import live.faceauth.sdk.ui.SettingsActivity;
import live.faceauth.sdk.util.Settings;

public class PreAuthenticateActivity extends AppCompatActivity
    implements FaceAuth.AuthenticationCallback {
  @BindView(R.id.phoneNumber) EditText phoneNumberView;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_pre_authenticate);
    ButterKnife.bind(this);

    String phoneNumber = getIntent().getStringExtra("PHONE_NUMBER");
    if (phoneNumber != null) {
      phoneNumberView.setText(phoneNumber);
    }
  }

  @OnClick(R.id.registerWrapper) public void onRegisterClick(View v) {
    startActivity(new Intent(this, PreRegisterActivity.class));
    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    finish();
  }

  @OnClick(R.id.authenticateButton) public void authenticate(View v) {
    boolean skip = false;
    String phoneNumber = phoneNumberView.getText().toString();
    if (validateInput(phoneNumber)) {
      if (skip) {
        openWelcomeActivity("Mohandas", phoneNumber, "", "");
        return;
      }
      if (Settings.getGestureMode(this)) {
        FaceAuth.getInstance().authenticateWithGestures(this, phoneNumber, true);
      } else {
        FaceAuth.getInstance().authenticateLegacy(this, phoneNumber, true);
      }
    }
  }

  private boolean validateInput(String phoneNumber) {
    phoneNumberView.setError(null);
    if (TextUtils.isEmpty(phoneNumber)) {
      phoneNumberView.setError(getString(R.string.enter_phone_number));
      return false;
    }
    return true;
  }

  @Override protected void onActivityResult(final int requestCode, final int resultCode,
      final Intent result) {
    boolean authResultHandled =
        FaceAuth.getInstance().handleAuthentication(requestCode, resultCode, result, this);

    if (!authResultHandled) {
      super.onActivityResult(requestCode, resultCode, result);
    }
  }

  @Override public void onSuccess(int confidence, double score, String name, String phoneNumber,
      String requestId, String imageUrl) {
    String message =
        String.format(getString(R.string.auth_success_msg), name, Integer.toString(confidence),
            Double.toString(score));
    showAuthResultDialog(message, true, name, phoneNumber, requestId, imageUrl);
  }

  @Override public void onFailure(int confidence, double score) {
    String message = String.format(getString(R.string.auth_failed_result_msg), confidence,
        Double.toString(score));
    showAuthResultDialog(message, false, "", "", "", "");
  }

  @Override public void onError(Exception e) {
    String message = String.format(getString(R.string.auth_failed_result), e.getMessage());
    showAuthResultDialog(message, false, "", "", "", "");
  }

  private void showAuthResultDialog(String message, final boolean success, final String name,
      final String phoneNumber, final String requestId, final String imageUrl) {
    new AlertDialog.Builder(this).setMessage(message)
        .setTitle(R.string.face_auth_res_title)
        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
          @Override public void onClick(DialogInterface dialogInterface, int i) {
            if (success) {
              openWelcomeActivity(name, phoneNumber, requestId, imageUrl);
            }
          }
        })
        .show();
  }

  private void openWelcomeActivity(String name, String phoneNumber, String requestId,
      String imageUrl) {
    Intent intent = new Intent(this, WelcomeActivity.class);
    intent.putExtra("NAME", name);
    intent.putExtra("PHONE_NUMBER", phoneNumber);
    intent.putExtra("REQUEST_ID", requestId);
    intent.putExtra("PROFILE_PICTURE", imageUrl);
    startActivity(intent);
    finish();
  }

  @OnClick(R.id.settingsButton) public void openSettings(View v) {
    startActivity(new Intent(this, SettingsActivity.class));
  }
}
