package live.faceauth.sdk.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import java.io.ByteArrayOutputStream;
import java.util.List;
import live.faceauth.sdk.R;
import live.faceauth.sdk.ui.camera.CameraSource;
import live.faceauth.sdk.ui.camera.CameraSourcePreview;
import live.faceauth.sdk.ui.camera.GraphicOverlay;
import live.faceauth.sdk.util.CameraManager;
import live.faceauth.sdk.util.FaceInfo;
import live.faceauth.sdk.util.ImageUtil;
import live.faceauth.sdk.util.PermissionUtil;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class KycDocCaptureActivity extends AppCompatActivity
    implements View.OnClickListener, CameraManager.PictureClickListener {
  private static final String TAG = "KycDocCaptureActivity";
  private static final int REQUEST_KYC_DOC_CAPTURE = 5;

  private ProgressDialog progressDialog;
  private GraphicOverlay graphicOverlay;
  private TextView topMessaging;
  private ImageButton captureButton;
  private ImageButton chooseImage;
  private ImageButton toggleCamera;
  private CameraManager cameraManager;
  private CameraSourcePreview preview;
  private FrameLayout mFrameBorder;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_kyc_doc_capture);
    identifyViews();
    attachListeners();
    createProgressDialog();
    startCamera();
  }

  private void startCamera() {
    cameraManager = new CameraManager(this, graphicOverlay, getWindowManager(), preview,
        CameraSource.CAMERA_FACING_BACK, this);
    List<String> permissions =
        PermissionUtil.checkMissingPermissions(this, CAMERA, WRITE_EXTERNAL_STORAGE,
            READ_EXTERNAL_STORAGE);
    if (permissions.isEmpty()) {
      cameraManager.createCameraSource();
    } else {
      finish();
    }
  }

  private void attachListeners() {
    captureButton.setOnClickListener(this);
    chooseImage.setOnClickListener(this);
    toggleCamera.setOnClickListener(this);
  }

  private void identifyViews() {
    preview = findViewById(R.id.preview);
    graphicOverlay = findViewById(R.id.faceOverlay);
    captureButton = findViewById(R.id.button_capture);
    chooseImage = findViewById(R.id.button_choose_image);
    toggleCamera = findViewById(R.id.toggle_camera);
    topMessaging = findViewById(R.id.top_messaging);
    mFrameBorder = findViewById(R.id.frame_border);

    // Hide unused views
    toggleCamera.setVisibility(View.GONE);
    chooseImage.setVisibility(View.GONE);
  }

  private void createProgressDialog() {
    progressDialog = new ProgressDialog(this);
    progressDialog.setCancelable(false);
    progressDialog.setMessage(getString(R.string.processing));
  }

  private void startRegisterConfirmActivity(Uri uri) {
    Intent confirmIntent = new Intent(this, KycDocCaptureConfirmActivity.class);
    confirmIntent.putExtras(getIntent());
    confirmIntent.setData(uri);
    startActivityForResult(confirmIntent, REQUEST_KYC_DOC_CAPTURE);
  }

  @Override public void onClick(View view) {
    int id = view.getId();
    if (id == captureButton.getId()) {
      captureImage();
    } else {
      finish();
    }
  }

  private void captureImage() {
    progressDialog.show();
    cameraManager.captureImage();
  }

  @Override protected void onActivityResult(final int requestCode, final int resultCode,
      final Intent result) {
    switch (requestCode) {
      case REQUEST_KYC_DOC_CAPTURE:
        if (resultCode == RESULT_OK) {
          setResult(RESULT_OK, result);
          finish();
        }
        break;
      default:
        super.onActivityResult(requestCode, resultCode, result);
        break;
    }
  }

  @Override public void onPictureClick(byte[] bytes) {
    Bitmap bitmap = ImageUtil.getBitmap(bytes);

    float imageWidth = bitmap.getWidth();
    float imageHeight = bitmap.getHeight();

    // Overlay is just a scaled version of image. It's quite possible that overlay expands
    // beyond the boundaries of the preview/screen.
    float overlayWidth = graphicOverlay.getWidth();
    float overlayHeight = graphicOverlay.getHeight();

    // First translate from preview to overlay coordinates
    // and then scale to image coordinates

    float shiftLeft = (preview.getWidth() - graphicOverlay.getWidth()) / 2.0f;
    float shiftTop = (preview.getHeight() - graphicOverlay.getHeight()) / 2.0f;

    float scaleX = imageWidth / overlayWidth;
    float scaleY = imageHeight / overlayHeight;

    int x = (int) ((mFrameBorder.getLeft() - shiftLeft) * scaleX);
    int y = (int) ((mFrameBorder.getTop() - shiftTop) * scaleY);

    int width = (int) (mFrameBorder.getWidth() * scaleX);
    int height = (int) (mFrameBorder.getHeight() * scaleY);

    try {
      ByteArrayOutputStream bos = new ByteArrayOutputStream();
      Bitmap bmp2 = Bitmap.createBitmap(bitmap, x, y, width, height);
      bmp2.compress(Bitmap.CompressFormat.JPEG, 100, bos);
      cameraManager.savePicture(bos.toByteArray());
      bos.close();
    } catch (Exception e) {
      Log.e(TAG, "Failed to cut face " + e.getMessage(), e);
      cameraManager.savePicture(bytes);
    }
  }

  @Override public void onPictureSave(Uri uri) {
    progressDialog.hide();
    startRegisterConfirmActivity(uri);
  }

  @Override public void updateFaces(SparseArray<FaceInfo> mListeners) {

  }

  @Override protected void onResume() {
    super.onResume();
    cameraManager.onResume();
  }

  @Override protected void onPause() {
    super.onPause();
    cameraManager.onPause();
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    cameraManager.onDestroy();
  }
}
