package live.faceauth.sdk.gesture;

import android.hardware.SensorEvent;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import com.google.android.gms.vision.face.Face;
import java.util.ArrayList;
import java.util.List;

import static live.faceauth.sdk.gesture.GestureTask.FINISH_TASK;
import static live.faceauth.sdk.gesture.GestureTask.PAUSE_TASK;

public class GestureController {

  private static final float TASK_COMPLETION_TIME_MS = 6.0f * 1000; // milliseconds
  private static GestureController sInstance;

  private List<GestureTask> mTasks;
  private List<GestureTask> mNegativeTasks;
  private int mCurrentTaskIndex = 0;

  private GestureController() {
    // use get()
  }

  public static GestureController get() {
    if (sInstance == null) {
      sInstance = new GestureController();
      sInstance.reset();
    }

    return sInstance;
  }

  public void reset() {
    createTasks();
    createNegativeTasks();
    mCurrentTaskIndex = 0;
  }

  private void createTasks() {
    final List<GestureTask> tasks = new ArrayList<>();
    tasks.add(new StabilizeTask());
    tasks.add(new BlinkTask());
    tasks.add(new PauseTask(0.5f));
    tasks.add(new ShakingTask());
    tasks.add(new PauseTask(0.5f));
    tasks.add(new NoddingTask());
    tasks.add(new PauseTask(0.5f));
    //tasks.add(new NoddingTask());
    tasks.add(new SmileTask());
    tasks.add(new PauseTask(0.5f));
    tasks.add(new FinishTask());
    mTasks = tasks;
  }

  private void createNegativeTasks() {
    final List<GestureTask> tasks = new ArrayList<>();
    tasks.add(new SmileTask());
    tasks.add(new BlinkTask());
    mNegativeTasks = tasks;
  }

  private List<GestureTask> getCurrentNegativeTasks() {
    String currentTaskName = getCurrentTask().getTaskName();
    List<GestureTask> tasks = new ArrayList<>();
    List<GestureTask> tasks2 = new ArrayList<>();

    if (FINISH_TASK.equals(currentTaskName) || PAUSE_TASK.equals(currentTaskName)) {
      return tasks;
    }

    for (GestureTask task : mNegativeTasks) {
      if (!task.getTaskName().equals(currentTaskName)) {
        tasks.add(task);
      }
    }

    return tasks2;
  }

  private void updateNegativeTasks(Face face) {
    for (GestureTask task : mNegativeTasks) {
      task.updateFace(face);
    }
  }

  public void start() {
    startTaskTracking();
    printStatus();
  }

  public void startTaskTracking() {
    createNegativeTasks();
    getCurrentTask().startTracking();
  }

  private void stopTaskTracking() {
    getCurrentTask().stopTracking();
  }

  public void updateFace(Face face, SensorEvent event) {
    getCurrentTask().updateFace(face);
    getCurrentTask().updateSensorEvent(event);
    updateNegativeTasks(face);
    if (shouldShowNext()) {
      stopTaskTracking();
      next();
    }
  }

  @StringRes public int getHintMessageId(boolean tooSmall) {
    return getCurrentTask().getHintMessageId(tooSmall);
  }

  @DrawableRes public int getSuggestionDrawable() {
    return getCurrentTask().getSuggestionDrawable();
  }

  public String getDebugMessage() {
    return getCurrentTask().getDebugMessage();
  }

  private void printStatus() {
    final int taskNum = mCurrentTaskIndex + 1;
    android.util.Log.d("GestureTask", "processing " + taskNum + " of " + size());
  }

  public boolean next() {
    mCurrentTaskIndex += 1;
    if (hasMoreTasks()) {
      printStatus();
      startTaskTracking();
      return true;
    } else {
      return false;
    }
  }

  public boolean hasFinished() {
    return FINISH_TASK.equals(getCurrentTask().getTaskName());
  }

  public void setFaceSmall(boolean tooSmall) {
    getCurrentTask().setFaceSmall(tooSmall);
  }

  public boolean shouldShowNext() {
    boolean negativeTaskHappened = false;
    String negativeTask = "";
    for (GestureTask task : getCurrentNegativeTasks()) {
      negativeTaskHappened = negativeTaskHappened || task.isLiveFace();
      negativeTask = task.getTaskName();
    }

    if (negativeTaskHappened) {
      getCurrentTask().setNegativeTask(negativeTask);
    }

    return !negativeTaskHappened && getCurrentTask().isLiveFace();
  }

  public GestureTask getCurrentTask() {
    return mTasks.get(mCurrentTaskIndex);
  }

  public int size() {
    return mTasks.size();
  }

  public boolean hasMoreTasks() {
    return mCurrentTaskIndex < size();
  }
}
