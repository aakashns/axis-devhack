package live.faceauth.sdk.models;

public class MatchResponse {

  public final boolean success;

  public final boolean isIdentical;

  public final double confidence;

  public final PersonalResponse personal;

  public MatchResponse(boolean success, boolean isIdentical, double confidence,
      PersonalResponse personal, String requestId) {
    this.success = success;
    this.isIdentical = isIdentical;
    this.confidence = confidence;
    this.personal = personal;
  }
}
