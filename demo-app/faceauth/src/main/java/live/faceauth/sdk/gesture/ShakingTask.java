package live.faceauth.sdk.gesture;

import android.graphics.PointF;
import android.support.annotation.Nullable;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.Landmark;
import java.util.List;
import live.faceauth.sdk.R;

public class ShakingTask extends GestureTask {

  private static final float EULER_Y_THRESHOLD = 14.0f;

  private float mEulerY;
  private float mEulerZ;

  private float mMinEulerY = 100.0f;
  private float mMaxEulerY = -100.0f;

  private Landmark mLeftEye;
  private Landmark mRightEye;
  private Landmark mLeftMouth;
  private Landmark mRightMouth;

  @Override String getTaskName() {
    return SHAKING_TASK;
  }

  private Landmark getLandmark(List<Landmark> landmarks, int type) {
    for (Landmark landmark : landmarks) {
      if (landmark.getType() == type) {
        return landmark;
      }
    }
    return null;
  }

  @Override void updateFace(Face face) {
    mEulerY = face.getEulerY();
    mEulerZ = face.getEulerZ();
    List<Landmark> landmarks = face.getLandmarks();

    mMaxEulerY = Math.max(mMaxEulerY, mEulerY);
    mMinEulerY = Math.min(mMinEulerY, mEulerY);

    mLeftEye = getLandmark(landmarks, Landmark.LEFT_EYE);
    mRightEye = getLandmark(landmarks, Landmark.RIGHT_EYE);
    mLeftMouth = getLandmark(landmarks, Landmark.LEFT_MOUTH);
    mRightMouth = getLandmark(landmarks, Landmark.RIGHT_MOUTH);
  }

  private float getEulerYDiff() {
    return mMaxEulerY - mMinEulerY;
  }

  @Override boolean isLiveFace() {
    return (getEulerYDiff() >= EULER_Y_THRESHOLD) && isFaceLargeEnough();
  }

  @Override int getHintMessageId(boolean tooSmall) {
    if (!isFaceLargeEnough()) {
      return R.string.tracker_face_too_small;
    } else {
      return R.string.tracker_sideways;
    }
  }

  @Override public int getSuggestionDrawable() {
    return R.drawable.sideways_anim;
  }

  private PointF getPosition(Landmark landmark) {
    if (landmark != null) {
      return landmark.getPosition();
    } else {
      return null;
    }
  }

  private float getDistance(Landmark landmark1, Landmark landmark2) {
    PointF point1 = getPosition(landmark1);
    PointF point2 = getPosition(landmark2);

    if (point1 != null & point2 != null) {
      return (float) Math.hypot(point1.x - point2.x, point1.y - point2.y);
    }
    return -1.0f;
  }

  @Nullable String getDebugMessage() {
    //return "EulerY: " + mEulerY + "\nEulerZ: " + mEulerZ;
    return "EulerYDiff: " + getEulerYDiff();
    //return
    //    "LeftEye: " + getPosition(mLeftEye) + "\n" +
    //    "RightEye: " + getPosition(mRightEye) + "\n" +
    //    "LeftMouth" + getPosition(mLeftMouth) + "\n" +
    //    "RightMouth" + getPosition(mRightMouth) + "\n";

    //return
    //    "LeftEyeMouth: " + getDistance(mLeftEye, mLeftMouth) + "\n" +
    //    "RightEyeMouth: " + getDistance(mRightEye, mRightMouth) + "\n" +
    //    "LeftRightEye" + getDistance(mLeftEye, mRightEye) + "\n" +
    //    "LeftRightMouth" + getDistance(mLeftMouth, mRightMouth) + "\n";
  }

  @Override void log() {

  }
}
