package live.faceauth.sdk.models;

public class SubmitKycResponse {

  public final boolean success;

  public final KycMatchResponse match;

  public final PersonalResponse personal;

  public final String requestId;

  public SubmitKycResponse(boolean success, KycMatchResponse match, String requestId,
      PersonalResponse personal) {
    this.success = success;
    this.match = match;
    this.requestId = requestId;
    this.personal = personal;
  }
}
