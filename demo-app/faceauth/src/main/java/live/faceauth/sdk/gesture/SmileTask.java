package live.faceauth.sdk.gesture;

import android.support.annotation.StringRes;
import com.google.android.gms.vision.face.Face;
import live.faceauth.sdk.FaceAuth;
import live.faceauth.sdk.R;

public class SmileTask extends GestureTask {

  private static final float SMILING_PROB_THRESHOLD = 0.2f;
  private static final float HAS_SMILED = 1.0F - SMILING_PROB_THRESHOLD;
  private static final float HAS_FROWNED = 0.0F + SMILING_PROB_THRESHOLD;

  private static final String LOG_TAG = "SmileTask";

  private float mMaxSmilingProbability = 0.0f;
  private float mMinSmilingProbability = 1.0f;

  @Override String getTaskName() {
    return SMILE_TASK;
  }

  @Override public void updateFace(Face face) {
    float smileProb = face.getIsSmilingProbability();

    if (smileProb > 0) {
      mMinSmilingProbability = Math.min(mMinSmilingProbability, smileProb);
      mMaxSmilingProbability = Math.max(mMaxSmilingProbability, smileProb);
    }
  }

  private float getSmilingProbDiff() {
    return mMaxSmilingProbability - mMinSmilingProbability;
  }

  @Override public boolean isLiveFace() {
    final boolean isLive;
    if (FaceAuth.getConfig().enableLivenessDetection) {
      isLive = getSmilingProbDiff() > SMILING_PROB_THRESHOLD;
    } else {
      isLive = true;
    }

    return isLive && isFaceLargeEnough();
  }

  @Override public int getSuggestionDrawable() {
    return R.drawable.face_smiling;
  }

  @StringRes @Override public int getHintMessageId(boolean tooSmall) {
    final int messageResId;
    if (!isFaceLargeEnough()) {
      return R.string.tracker_face_too_small;
    } else if (mMaxSmilingProbability > HAS_SMILED) {
      messageResId = R.string.tracker_dont_smile;
    } else if (mMinSmilingProbability < HAS_FROWNED) {
      messageResId = R.string.tracker_smile;
    } else {
      messageResId = R.string.tracker_change_expressions;
    }

    return messageResId;
  }

  @Override public void log() {
    long duration = mTrackingTimeEnd - mTrackingTimeStart;
    float smilingDiff = getSmilingProbDiff();

    android.util.Log.d(LOG_TAG, "Tracked between: " + mTrackingTimeStart + "," + mTrackingTimeEnd);
    android.util.Log.d(LOG_TAG, "Tracked duration: " + duration);

    android.util.Log.d(LOG_TAG,
        "Smiling prob: " + mMinSmilingProbability + "," + mMaxSmilingProbability);
    android.util.Log.d(LOG_TAG, "Smiling diff: " + smilingDiff);
  }
}
