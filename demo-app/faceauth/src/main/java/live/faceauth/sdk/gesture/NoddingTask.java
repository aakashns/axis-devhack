package live.faceauth.sdk.gesture;

import android.support.annotation.Nullable;
import com.google.android.gms.vision.face.Face;
import live.faceauth.sdk.R;

public class NoddingTask extends GestureTask {

  private static final float EULER_Z_THRESHOLD = 10.0f;

  private float mEulerZ;

  private float mMinEulerZ = 100.0f;
  private float mMaxEulerZ = -100.0f;

  @Override String getTaskName() {
    return NODDING_TASK;
  }

  @Override void updateFace(Face face) {
    mEulerZ = face.getEulerZ();

    mMaxEulerZ = Math.max(mMaxEulerZ, mEulerZ);
    mMinEulerZ = Math.min(mMinEulerZ, mEulerZ);
  }

  private float getEulerZDiff() {
    return mMaxEulerZ - mMinEulerZ;
  }

  @Override boolean isLiveFace() {
    return (getEulerZDiff() >= EULER_Z_THRESHOLD) && isFaceLargeEnough();
  }

  @Override int getHintMessageId(boolean tooSmall) {
    if (!isFaceLargeEnough()) {
      return R.string.tracker_face_too_small;
    } else {
      return R.string.tracker_nodding;
    }
  }

  @Override public int getSuggestionDrawable() {
    return R.drawable.tilt_anim;
  }

  @Nullable String getDebugMessage() {
    return "EulerZDiff: " + getEulerZDiff();
  }

  @Override void log() {

  }
}
